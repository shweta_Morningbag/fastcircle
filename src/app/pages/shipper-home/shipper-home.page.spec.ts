import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ShipperHomePage } from './shipper-home.page';

describe('ShipperHomePage', () => {
  let component: ShipperHomePage;
  let fixture: ComponentFixture<ShipperHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipperHomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ShipperHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
