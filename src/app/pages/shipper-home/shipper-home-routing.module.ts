import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShipperHomePage } from './shipper-home.page';

const routes: Routes = [
  {
    path: '',
    component: ShipperHomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShipperHomePageRoutingModule {}
