import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShipperHomePageRoutingModule } from './shipper-home-routing.module';

import { ShipperHomePage } from './shipper-home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShipperHomePageRoutingModule
  ],
  declarations: [ShipperHomePage]
})
export class ShipperHomePageModule {}
