import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShipperRegistrationPage } from './shipper-registration.page';

const routes: Routes = [
  {
    path: '',
    component: ShipperRegistrationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShipperRegistrationPageRoutingModule {}
