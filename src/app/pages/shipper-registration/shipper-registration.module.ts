import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShipperRegistrationPageRoutingModule } from './shipper-registration-routing.module';

import { ShipperRegistrationPage } from './shipper-registration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShipperRegistrationPageRoutingModule
  ],
  declarations: [ShipperRegistrationPage]
})
export class ShipperRegistrationPageModule {}
