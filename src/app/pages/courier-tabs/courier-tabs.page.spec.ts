import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CourierTabsPage } from './courier-tabs.page';

describe('CourierTabsPage', () => {
  let component: CourierTabsPage;
  let fixture: ComponentFixture<CourierTabsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourierTabsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CourierTabsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
