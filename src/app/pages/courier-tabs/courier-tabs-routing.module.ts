import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CourierTabsPage } from './courier-tabs.page';

const routes: Routes = [
  {
    path: 'courier-tabs',
    component: CourierTabsPage,
    children: [
      {
        path: 'shipper-home',
        children: [
          {
            path: '',
              loadChildren: () => 
              import('../shipper-home/shipper-home.module').then( m => m.ShipperHomePageModule)
          }
        ]
      },
      {
        path: 'available-order',
        children: [
          {
            path: '',
            loadChildren: () => import('../available-order/available-order.module').then( m => m.AvailableOrderPageModule)
          }
        ]
      },
          {
        path: 'history',
        children: [
          {
            path: '',
            loadChildren: () => 
            import('../history/history.module').then( m => m.HistoryPageModule)
          }
        ]
      },
        {
        path: 'account',
        children: [
          {
            path: '',
            loadChildren: () => import('../account/account.module').then( m => m.AccountPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/courier-tabs/shipper-home',
        pathMatch: 'full'
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CourierTabsPageRoutingModule {}
