import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CourierTabsPageRoutingModule } from './courier-tabs-routing.module';

import { CourierTabsPage } from './courier-tabs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CourierTabsPageRoutingModule
  ],
  declarations: [CourierTabsPage]
})
export class CourierTabsPageModule {}
