import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AvailableOrderPageRoutingModule } from './available-order-routing.module';

import { AvailableOrderPage } from './available-order.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AvailableOrderPageRoutingModule
  ],
  declarations: [AvailableOrderPage]
})
export class AvailableOrderPageModule {}
