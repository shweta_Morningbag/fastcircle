import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AvailableOrderPage } from './available-order.page';

const routes: Routes = [
  {
    path: '',
    component: AvailableOrderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AvailableOrderPageRoutingModule {}
