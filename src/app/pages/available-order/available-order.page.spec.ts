import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AvailableOrderPage } from './available-order.page';

describe('AvailableOrderPage', () => {
  let component: AvailableOrderPage;
  let fixture: ComponentFixture<AvailableOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvailableOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AvailableOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
