import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MileRegistrationPageRoutingModule } from './mile-registration-routing.module';

import { MileRegistrationPage } from './mile-registration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MileRegistrationPageRoutingModule
  ],
  declarations: [MileRegistrationPage]
})
export class MileRegistrationPageModule {}
