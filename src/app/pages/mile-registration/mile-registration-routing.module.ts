import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MileRegistrationPage } from './mile-registration.page';

const routes: Routes = [
  {
    path: '',
    component: MileRegistrationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MileRegistrationPageRoutingModule {}
