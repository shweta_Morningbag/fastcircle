import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabPage } from './tab.page';

const routes: Routes =[
  {
    path: 'tab',
    component: TabPage,
    children: [
      {
        path: 'shipper-home',
        children: [
          {
            path: '',
              loadChildren: () => 
              import('../shipper-home/shipper-home.module').then( m => m.ShipperHomePageModule)
          }
        ]
      },
          {
        path: 'history',
        children: [
          {
            path: '',
            loadChildren: () => 
            import('../history/history.module').then( m => m.HistoryPageModule)
          }
        ]
      },
        {
        path: 'account',
        children: [
          {
            path: '',
            loadChildren: () => import('../account/account.module').then( m => m.AccountPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tab/shipper-home',
        pathMatch: 'full'
      }
    ]
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabPageRoutingModule {}
