import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CourierRegistrationPageRoutingModule } from './courier-registration-routing.module';

import { CourierRegistrationPage } from './courier-registration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CourierRegistrationPageRoutingModule
  ],
  declarations: [CourierRegistrationPage]
})
export class CourierRegistrationPageModule {}
