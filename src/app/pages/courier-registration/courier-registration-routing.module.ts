import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CourierRegistrationPage } from './courier-registration.page';

const routes: Routes = [
  {
    path: '',
    component: CourierRegistrationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CourierRegistrationPageRoutingModule {}
