import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(public alertController: AlertController) { }

  ngOnInit() {
  }
  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      header: 'Reset Password',
      message: "We will send you email with a code to reset your password. Code will be valid for 24 hours.",
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'Email'
        },
      ],
      buttons: [
       {
          text: 'Submit',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }
}
