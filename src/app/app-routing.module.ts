import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '',
  loadChildren: () => import('./pages/tab/tab.module').then( m => m.TabPageModule)
},
//   { path: '',
//   loadChildren: () => import('./pages/courier-tabs/courier-tabs.module').then( m => m.CourierTabsPageModule)
// },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./pages/sign-up/sign-up.module').then( m => m.SignUpPageModule)
  },
  {
    path: 'shipper-registration',
    loadChildren: () => import('./pages/shipper-registration/shipper-registration.module').then( m => m.ShipperRegistrationPageModule)
  },
  {
    path: 'registration-success',
    loadChildren: () => import('./pages/registration-success/registration-success.module').then( m => m.RegistrationSuccessPageModule)
  },
  {
    path: 'shipper-home',
    loadChildren: () => import('./pages/shipper-home/shipper-home.module').then( m => m.ShipperHomePageModule)
  },
  
  {
    path: 'history',
    loadChildren: () => import('./pages/history/history.module').then( m => m.HistoryPageModule)
  },
  {
    path: 'tab',
    loadChildren: () => import('./pages/tab/tab.module').then( m => m.TabPageModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then( m => m.AccountPageModule)
  },
  {
    path: 'modal',
    loadChildren: () => import('./pages/modal/modal.module').then( m => m.ModalPageModule)
  },
  {
    path: 'courier-registration',
    loadChildren: () => import('./pages/courier-registration/courier-registration.module').then( m => m.CourierRegistrationPageModule)
  },
  {
    path: 'available-order',
    loadChildren: () => import('./pages/available-order/available-order.module').then( m => m.AvailableOrderPageModule)
  },
  {
    path: 'mile-registration',
    loadChildren: () => import('./pages/mile-registration/mile-registration.module').then( m => m.MileRegistrationPageModule)
  },
  {
    path: 'courier-tabs',
    loadChildren: () => import('./pages/courier-tabs/courier-tabs.module').then( m => m.CourierTabsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
